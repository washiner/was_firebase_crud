import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Flutter Firebase',
        theme: ThemeData(
            primarySwatch: Colors.purple, primaryColor: Colors.purple),
        home: Home());
  }
}

class Home extends StatefulWidget {
  const Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  TextEditingController _produtoDigitado = TextEditingController();
  TextEditingController _valorDigitado = TextEditingController();

  CollectionReference _protudoNovo =
      FirebaseFirestore.instance.collection("produto");
  String escolha = "novo";
  Future _novoOuModifica([DocumentSnapshot novoValorSnapshot]) async {

    if (novoValorSnapshot != null) {
      escolha = "Modificar";
      _produtoDigitado.text = novoValorSnapshot["produto"];
      _valorDigitado.text = novoValorSnapshot["valor"].toString();

      showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text("Cadastro de Produto"),
              content: Container(
                height: 150,
                child: Column(
                  children: [
                    TextFormField(
                      controller: _produtoDigitado,
                      decoration: InputDecoration(
                          labelText: "Digite novo produto",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16))),
                    ),
                    Padding(padding: EdgeInsets.only(top: 20)),
                    TextFormField(
                      controller: _valorDigitado,
                      decoration: InputDecoration(
                          labelText: "Digite o preço",
                          border: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(16))),
                    ),
                  ],
                ),
              ),
              actions: [
                TextButton(
                  child: Text(escolha == "novo" ? "Novo " : "Modificar", style: TextStyle(fontSize: 20),),
                  onPressed: () async {
                    final String produto = _produtoDigitado.text;
                    final double valor =
                    double.tryParse(_valorDigitado.text);
                    if (produto != null && valor != null) {
                      if (escolha == "novo") {
                        await _protudoNovo.add({
                          "produto": produto,
                          "valor": valor,
                        });
                      }
                      if (escolha == "Modificar") {
                        //var novoValorSnapshot;
                        await _protudoNovo
                            .doc(novoValorSnapshot.id)
                            .update({"produto": produto, "valor": valor});
                      }
                      _produtoDigitado.text = "";
                      _valorDigitado.text = "";
                      Navigator.pop(context);
                    }
                  },
                ),
                TextButton(
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    child: Text(
                      "Sair",
                      style: TextStyle(fontSize: 20),
                    )),
              ],
            );
          });

    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text("Was Firebase"),
        ),
        body: StreamBuilder(
            stream: _protudoNovo.snapshots(),
            builder: (context, streamSnapshot) {
              if (streamSnapshot.hasData) {
                return ListView.builder(
                    itemCount: streamSnapshot.data.docs.length,
                    itemBuilder: (contex, index) {
                      final DocumentSnapshot documentoSnapshot =
                          streamSnapshot.data.docs[index];
                      return Card(
                        margin: EdgeInsets.all(5),
                        child: ListTile(
                          title: Text(documentoSnapshot["produto"]),
                          subtitle: Text(documentoSnapshot["valor"].toString()),
                          trailing: SizedBox(
                            width: 100,
                            child: Row(
                              children: [
                                IconButton(
                                    icon: Icon(Icons.edit),
                                    onPressed: () =>_novoOuModifica(documentoSnapshot)),
                                IconButton(
                                    icon: Icon(Icons.delete),
                                    onPressed: () =>
                                        _deleteProduct(documentoSnapshot.id))
                              ],
                            ),
                          ),
                        ),
                      );
                    });
              }
              return Center(
                child: CircularProgressIndicator(),
              );
            }),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.add),
          onPressed: () {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    title: Text("Cadastro de Produto"),
                    content: Container(
                      height: 150,
                      child: Column(
                        children: [
                          TextFormField(
                            controller: _produtoDigitado,
                            decoration: InputDecoration(
                                labelText: "Digite novo produto",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16))),
                          ),
                          Padding(padding: EdgeInsets.only(top: 20)),
                          TextFormField(
                            controller: _valorDigitado,
                            decoration: InputDecoration(
                                labelText: "Digite o preço",
                                border: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(16))),
                          ),
                        ],
                      ),
                    ),
                    actions: [
                      TextButton(
                        child: Text(escolha == "novo" ? "Novo " : "Modificar", style: TextStyle(fontSize: 20),),
                        onPressed: () async {
                          final String produto = _produtoDigitado.text;
                          final double valor =
                              double.tryParse(_valorDigitado.text);
                          if (produto != null && valor != null) {
                            if (escolha == "novo") {
                              await _protudoNovo.add({
                                "produto": produto,
                                "valor": valor,
                              });
                            }
                            if (escolha == "modificar") {
                              var novoValorSnapshot;
                              await _protudoNovo
                                  .doc(novoValorSnapshot.id)
                                  .update({"produto": produto, "valor": valor});
                            }
                            _produtoDigitado.text = "";
                            _valorDigitado.text = "";
                            Navigator.pop(context);
                          }
                        },
                      ),
                      TextButton(
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Text(
                            "Sair",
                            style: TextStyle(fontSize: 20),
                          )),
                    ],
                  );
                });
          },
        ));
  }

  Future<void> _deleteProduct(String productId) async {
    await _protudoNovo.doc(productId).delete();

    // Show a snackbar
    ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(content: Text('Produto deletado com Sucesso!!!')));
  }
}
